package services

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/valyala/fasthttp"
	"gitlab.com/swarm-mode/api/db"
	"gitlab.com/swarm-mode/api/res"
)

type HomeController struct{}

func (this *HomeController) Get(ctx *fasthttp.RequestCtx) {
	conn := fmt.Sprintf("%s:%s@%s?charset=utf8mb4&parseTime=True", res.Configs.GetString("db.username"), res.Configs.GetString("db.password"), res.Configs.GetString("db.host"))
	dbc, err := gorm.Open("mysql", conn)

	if err != nil {
		fmt.Fprintf(ctx, "Error on connecting to database. (%s) (%s)", err.Error(), conn)
		return
	}
	defer dbc.Close()
	dbc = dbc.AutoMigrate(&db.Counter{})

	if err := dbc.Create(&db.Counter{}).Error; err != nil {
		fmt.Fprintf(ctx, "Failed to increase counter. (%s)", err.Error())
		return
	}

	var counter int
	if err := dbc.Model(&db.Counter{}).Count(&counter).Error; err != nil {
		fmt.Fprintf(ctx, "Failed to retrieve counter. (%s)", err.Error())
		return
	}

	fmt.Fprintf(ctx, "Hooray!!!!! Counter: %d", counter)
}
