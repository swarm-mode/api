package main

import (
	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"gitlab.com/swarm-mode/api/services"
	"log"
)

func main() {
	router := fasthttprouter.New()

	server := fasthttp.Server{
		Handler:            router.Handler,
		MaxRequestBodySize: 10 * 1024 * 1024, // 10MB
	}

	home := services.HomeController{}

	router.GET("/", home.Get)

	log.Panic(server.ListenAndServe(":8080"))
}
