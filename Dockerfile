# Build Environment
FROM golang:latest as builder

ENV MYDIR="/go/src/gitlab.com/swarm-mode/api"

RUN mkdir -p $MYDIR
WORKDIR $MYDIR
ADD . .
RUN pwd && CGO_ENABLED=0 GOOS=linux  go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o main . && ls

# Production Environment
FROM alpine:latest

LABEL maintainer="Swarm mode <admin@dani-app.com>"
ENV MYDIR="/go/src/gitlab.com/swarm-mode/api"

#RUN adduser -S -D -H -h /app appuser
#USER appuser

COPY --from=builder $MYDIR/main /app/
COPY --from=builder $MYDIR/configs.json /app/configs.json

WORKDIR /app
RUN ls
CMD ["./main"]

#   https://www.cloudreach.com/blog/containerize-this-golang-dockerfiles/
#   https://blog.codeship.com/building-minimal-docker-containers-for-go-applications/
#   https://www.callicoder.com/docker-golang-image-container-example
