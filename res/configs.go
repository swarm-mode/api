package res

import (
	"errors"
	"github.com/spf13/viper"
)

var Configs *viper.Viper

var ConfigFileNotFoundError = errors.New("Config found not found")

func init() {
	Configs = viper.New()
	Configs.SetConfigName("configs")
	Configs.SetConfigType("json")
	Configs.AddConfigPath(".")
	Configs.AddConfigPath("../")
	Configs.SetEnvPrefix("env")
	Configs.AutomaticEnv()

	if err := Configs.ReadInConfig(); err != nil {
		panic("Configs.init > Failed to read the \"configs.json\" file: " + err.Error())
	}
}
